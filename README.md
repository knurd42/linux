# Thorsten's Linux repository

This repository holds the Linux sources and various related things:

* [arkify](https://gitlab.com/knurd42/linux/-/tree/arkify-arkify), which
  adds additional make targets to a Linux Git repository that among others
  allow building Fedora SRPMs from a branch holding the Linux kernel
  sources. The code realizing this is a slightly modified variant of the
  [kernel-ark infrastructure](https://cki-project.gitlab.io/kernel-ark/)
  the Fedora project uses to build all its kernels; packages built from the
  SRPMs created with this code thus should fit well into a modern Fedora.

* [arkify-infra-mainline](https://gitlab.com/knurd42/linux/-/tree/arkify-infra-mainline)
  and other branches with the prefix *arkify-infra-*. These hold the kernel-ark
  infrastructure arkify imports.

* [arkify-fixes-mainline](https://gitlab.com/knurd42/linux/-/tree/arkify-fixes-mainline)
  and other branches with the prefix *arkify-fixes-*. They are usually empty,
  but when needed hold Linux kernel patches required to make the SRPMs build
  with the kernel-ark infrastructure compile.

* [kvc-infra-mainline](https://gitlab.com/knurd42/linux/-/tree/kvc-infra-mainline)
  and other branches with the prefix *kvc-infra-*. They are downsteams of the
  *arkify-infra-* branches and hold adjustments to the kernel-ark infrastructure
  fpr the packages shipped by the [kernel vanilla repositories for Fedora](https://fedoraproject.org/wiki/Kernel_Vanilla_Repositories).

* [kvc-vanilla-mainline](https://gitlab.com/knurd42/linux/-/tree/kvc-vanilla-mainline),
  and other branches with the prefix *kvc-vanilla-*. They contain combinations
  of a upstream Linux tree, fixes from a *arkify-fixes-* branch, and the
  kernel-ark infrastructure from a *kvc-infra-* branch that by tagging was sent
  to build for the kernel vanilla repositories for Fedora Linux.

* [kvc-scripts](https://gitlab.com/knurd42/linux/-/tree/kvc-scripts), which
  occasionally updates the *arkify-infra-*, *arkify-fixes-*, and *kvc-infra-*
  branches and then combines the code into
  [a updated *kvc-vanilla-* branch](https://fedoraproject.org/wiki/Kernel_Vanilla_Repositories-FAQ#Where_is_the_code_used_to_build_the_packages?),
  and tags a release for building.

* Various branches holding upstream sources of Linux

* Various branches with forks of Linux or kernel-ark containing changes sent
  for upstreaming.

The repo is a fork of [kernel-ark](https://gitlab.com/cki-project/kernel-ark),
which itself is a fork of [Linux mainline](https://gitlab.com/linux-kernel/linux).
